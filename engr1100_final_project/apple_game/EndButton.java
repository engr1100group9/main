import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EndButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EndButton extends Actor
{
    /**
     * Act - do whatever the EndButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GreenfootImage endGame = new GreenfootImage ("Click this to end game", 12 ,Color.RED,Color.WHITE);
        setImage(endGame);
         if (Greenfoot.mouseClicked(null)){
             Greenfoot.setWorld(new EndScreen());
        }
    }    
}
