import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EndScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EndScreen extends World
{
    private int stopCounter = 15;
    /**
     * Constructor for objects of class EndScreen.
     * 
     */
    public EndScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
    }
    public void act(){
            GreenfootSound gameOverSound = new GreenfootSound("gameoversound.mp3");
            gameOverSound.setVolume(50);
            gameOverSound.play();
            setPaintOrder(GameOver.class,RegularApple.class, GreenApple.class, Pizza.class,
                        Basket.class, PlayerCharacter.class);
            addObject(new GameOver(), 300,200);  
            if(stopCounter > 0){
                stopCounter = stopCounter -1;
                if(stopCounter ==0){
                   Greenfoot.stop();
                }
            }
    }
}
