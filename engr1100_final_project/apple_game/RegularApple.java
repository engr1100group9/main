import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RegularApple here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RegularApple extends Actor
{
    int RAppleHit = ((MyWorld) getWorld()).redApplesMissed;
    
    public RegularApple() {
        
    }
    
    /**
     * Act - do whatever the RegularApple wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        setLocation(getX(), getY() + 1);
        if (getY() == 380) {
            //Greenfoot.stop();
            getWorld().removeObject(this);
            MyWorld.updateRedApplesMissed();
            MyWorld.updateTotalLives();
            //MyWorld.resetStaticVariables();
        }
    }    
}
