import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GameOver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameOver extends Actor
{
    /**
     * Act - do whatever the GameOver wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GreenfootImage gameOver = new GreenfootImage ("GAME OVER \n Your total score is: " + MyWorld.getTotalScore(), 30 ,Color.RED,Color.BLACK);
        setImage(gameOver);
    }    
}
