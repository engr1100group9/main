import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class StartScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class StartScreen extends World
{

    /**
     * Constructor for objects of class StartScreen.
     * 
     */
    public StartScreen()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1); 
        Greenfoot.start();
        //GreenfootImage background = getBackground();
        //background.setColor(Color.BLACK);
        //setBackground(background);
        addObject(new Directions(), 300,200);
        //showText("instructions", 300, 100);
        //GreenfootImage text =new GreenfootImage ("instructions", 25 ,Color.RED,Color.RED);
        //text.setLocation(200,300); 
    }
    public void act(){
        if (Greenfoot.isKeyDown("space")){
            Greenfoot.setWorld(new MyWorld());
            MyWorld.resetStaticVariables();
        }
    }
}
