import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends greenfoot.World
{
    public static final int INITIAL_SPEED = 50;
    public static final int INITIAL_LIVES = 30;
    
    public static int redApplesCaught = 0;
    public static int greenApplesCaught = 0;
    public static int greenScore = 0;
    public static int TotalScore = 0;
    public static int Lives = INITIAL_LIVES;
    public static int redApplesMissed = 0;
    public static int pizzaHit = 0;
    private GreenfootSound gameMusic;
    
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1, false); 
        GreenfootImage bg = new GreenfootImage("betaBackGround.PNG");
        gameMusic = new GreenfootSound("music_david_gwyn_jones_looking_back_over_the_hill_instrumental.mp3");
        bg.scale(600,400);
        setBackground(bg);
        addObject(new Basket(), 300, 330);
        addObject(new PlayerCharacter(), 300, 300);
        addObject(new Score(), 70,30);
        addObject(new LifeScore(), 70, 50);
        addObject(new EndButton(), 540, 20);
        setPaintOrder(RegularApple.class, GreenApple.class, Pizza.class,
                        Basket.class, PlayerCharacter.class);
    }
    public static int getRedApplesCaught(){
        return redApplesCaught;
    }
    public static int getGreenApplesCaught(){
        return greenApplesCaught;
    }
    public static int getTotalScore(){
        return TotalScore;
    }
    public static void updateRedApple(){
        redApplesCaught++;
    }
    public static void updateGreenApple(){
        greenApplesCaught++;
    }
    public static void updateTotalScore(){
        greenScore = greenApplesCaught * 5;
        TotalScore = redApplesCaught + greenScore;
        
    }
    public static int getRedApplesMissed(){
        return redApplesMissed;
    }
    public static void updateRedApplesMissed(){
        redApplesMissed = redApplesMissed + 1;
    }
    public static int PizzaHit(){
        return pizzaHit;
    }
    public static void updatePizzaHit(){
        pizzaHit = pizzaHit + 1;
    }
    public static void updateTotalLives(){
        Lives = INITIAL_LIVES - redApplesMissed - pizzaHit;
        
    }
    public static int getTotalLives(){
        return Lives;
    }
    
    public void act() {
        int x = Greenfoot.getRandomNumber(600);
        int y = Greenfoot.getRandomNumber(20);
        
        gameMusic.playLoop();
        
        if (Greenfoot.getRandomNumber(1000) < 3) {
            addObject(new RegularApple(), x, y);
        }
        if (Greenfoot.getRandomNumber(1500) < 3) {
            addObject(new GreenApple(), x, y);
            GreenfootSound sparkle = new GreenfootSound("sparkle.mp3");
            sparkle.setVolume(30);
            sparkle.play();
        }
        if (Greenfoot.getRandomNumber(1000) < 2) {
            addObject(new Pizza(), x, y);
        }
        if(Lives <= 0){
            Greenfoot.setWorld(new EndScreen());
        }
        if (TotalScore > 9 && TotalScore % 10 == 0) {
            int speed = INITIAL_SPEED + (TotalScore / 10);
            Greenfoot.setSpeed(speed);
        }
    }
    public static void resetStaticVariables(){
        Greenfoot.setSpeed(INITIAL_SPEED);    
        redApplesCaught = 0;
        greenApplesCaught = 0;
        greenScore = 0;
        TotalScore = 0;
        Lives = INITIAL_LIVES;
        redApplesMissed = 0;
        pizzaHit = 0;   
        }
}
