import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RedAppleScore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RedAppleScore extends Actor
{
    private int score = 0;
    public RedAppleScore(){
        GreenfootImage redBoard = new GreenfootImage(200, 30);
        redBoard.drawString("Red Apples Caught: " + score, 5, 25);
        setImage(redBoard);
    }
    /**
     * Act - do whatever the RedAppleScore wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
    //public void changeRedScore(){
        //score = score + 1;
        //GreenfootImage RedBoard = getImage();
        //RedBoard.clear();
        //RedBoard.drawString("Red Apples Caught: " + score, 5, 25);
   // }

}
