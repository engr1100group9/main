import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
//import java.awt.Color;
//import Greenfoot.Color;
/**
 * Write a description of class Instructions here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Directions extends Actor
{
    
    /**
     * Act - do whatever the Instructions wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GreenfootImage text =new GreenfootImage ("The Goal: to obtain as many points as you can by catching apples \n while avoiding pizza\n -Red apples are worth 1 point each while green apples are worth 5 \n-There is no penalty for letting a green apple touch the ground but \nletting a red apple touch the ground will cost you one life \n-Touching the pizza will also make you loose a life                           \n-You have 100 Lives before it's game over                                         \n-Control the player by using a and d or the left and right arrow keys\n-Hit the space bar when your ready to play and good luck!", 20 ,Color.RED,Color.WHITE);
        setImage(text);
    }    
    public void Instructions(){
       //GreenfootImage text =new GreenfootImage ("instructions", 25 ,Color.RED,Color.RED);
       //setLocation(300,300);
    }
}