import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class LifeScore here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class LifeScore extends Actor
{
    /**
     * Act - do whatever the LifeScore wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        GreenfootImage Lscore = new GreenfootImage ("Lives: " + MyWorld.getTotalLives(), 20 ,Color.RED,Color.WHITE);
        setImage(Lscore);
    }    
}
